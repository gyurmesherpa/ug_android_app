package com.gyurme;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gyurme.dao.DAO;
import com.gyurme.dao.DAOImpl;
import com.gyurme.dao.SqliteHelper;
import com.gyurme.models.Farm;
import com.gyurme.webrequests.RequestPackage;
import com.gyurme.webrequests.WebHttpRequest;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class MainActivity extends AppCompatActivity {
    Context context = this;
    DAO dao = new DAOImpl();
    public static String serverUriAndPort = "http://192.168.1.69:8080/gyurmeUGServer";
    static String webResponse;
    // constant to compare
    // the activity result code
    int SELECT_PICTURE = 200;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        final Handler mHandler = new Handler(Looper.getMainLooper()){

            public void handleMessage(android.os.Message msg) {
                Bundle b = null;
                if(msg.what==1){
                    b=msg.getData();
                    if(b.getString("farmRegisterResponse").equals("Account Registered")){
                        Toast.makeText(context,b.getString("farmRegisterResponse"),Toast.LENGTH_LONG).show();
                        setContentView(R.layout.activity_main);
                        //fill account info from db
                        TextView farmNameField = findViewById(R.id.loginFarmNameField);
                        farmNameField.setText(DAOImpl.getFarmName());
                        EditText farmEmailField = findViewById(R.id.loginEmailField);
                        farmEmailField.setText(DAOImpl.getFarmEmail());
                        EditText farmPwdField = findViewById(R.id.loginPwdField);
                        ((Button) findViewById(R.id.btnLogin)).setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                //Toast.makeText(context,DAOImpl.getFarmName(),Toast.LENGTH_LONG).show();
                                if(notFistLogin()){
                                    //comapre hashed pwd with local db pwd
                                    if(getSHA256(farmPwdField.getText().toString()).equals(DAOImpl.getFarmPwd())){
                                        mainPane();
                                    }
                                    else{
                                        Toast.makeText(context,"Incorrect password. Please, correct and try again",Toast.LENGTH_SHORT).show();
                                    }
                                }
                                else {

                                    Toast.makeText(context,"app logic error", Toast.LENGTH_SHORT).show();
                                }

                            }
                        });
                    }
                    else{
                        Toast.makeText(context,b.getString("farmRegisterResponse"),Toast.LENGTH_LONG).show();
                    }

                }
                else if(msg.what == 2){
                    b = msg.getData();
                    if(b.getString("accountSearchResponse").equals("Account found and details fetched")){
                        Toast.makeText(context,"Account found and Logged in",Toast.LENGTH_LONG).show();
                        finish();
                        mainPane();
                    }
                    else{
                        Toast.makeText(context,b.getString("accountSearchResponse"),Toast.LENGTH_LONG).show();
                    }

                }
                super.handleMessage(msg);
            }
        };

        DAOImpl.sqLiteDatabase = new SqliteHelper(this).getWritableDatabase();

        //check if this is the first installation run, if so:
        if(isFirstSetup() || notFistLogin() == false){
            setContentView(R.layout.layout_activity_main_install);

            if (this.dao.setDefaultAppEnvironment() == -1) {
                ToastMessage.message(this.context, "Failed to create default environment values");
            } else {
                dao.insertInitialTransactionTypes();
                ToastMessage.message(this.context, "Successfully created default environment values");
            }

            ((Button) findViewById(R.id.btnToRegister)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    setContentView(R.layout.layout_register_user);
                    ((Button) findViewById(R.id.btnRegisterAccount)).setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            //the fields
                            EditText farmNameField = findViewById(R.id.fieldFarmName);
                            String farmName = farmNameField.getText().toString();
                            EditText farmEmailField = findViewById(R.id.fieldFarmEmail);
                            String farmEmail = farmEmailField.getText().toString();
                            EditText farmPwdField = findViewById(R.id.fieldPwd);
                            String farmPwd = farmPwdField.getText().toString();
                            EditText farmConfirmPwdField = findViewById(R.id.fieldConfirmPwd);
                            String farmConfirmPwd = farmConfirmPwdField.getText().toString();
                            EditText farmPhoneNoField = findViewById(R.id.fieldFarmPhone);
                            String farmPhoneNo = farmPhoneNoField.getText().toString();
                            String hashedPwd = getSHA256(farmPwd);
                            EditText districtField = findViewById(R.id.districtField);
                            String district = districtField.getText().toString();
                            EditText plusCodeField = findViewById(R.id.farmPlusCodeField);
                            String plusCode = plusCodeField.getText().toString();
                            //Toast.makeText(context,farmNameField.getText().toString(),Toast.LENGTH_SHORT).show();
                            if(!farmPwd.equals(farmConfirmPwd) || farmPwd.length() == 0){
                                Toast.makeText(context,"The passwords do not match. And password should not be of length nill.",Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Thread thread = new Thread()
                                {
                                    @Override
                                    public void run() {
                                        try {
                                            boolean run = true;
                                            while(run) {
                                                WebHttpRequest whr = new WebHttpRequest();
                                                try {
                                                    Map<String, String> reqParams = new HashMap<>();
                                                    reqParams.put("r","register_farm");
                                                    reqParams.put("farm_name",farmNameField.getText().toString());
                                                    reqParams.put("email", farmEmailField.getText().toString());
                                                    reqParams.put("pwd", hashedPwd);
                                                    reqParams.put("phone_no",farmPhoneNoField.getText().toString());
                                                    reqParams.put("district",districtField.getText().toString());
                                                    reqParams.put("plus_code",plusCodeField.getText().toString());
                                                    //RequestPackage requestPackage = new RequestPackage();
                                                    //requestPackage.setMethod("POST");
                                                    //requestPackage.setParams(reqParams);
                                                    String uri = serverUriAndPort + "/farm";
                                                    webResponse = WebHttpRequest.generalPost(uri,reqParams);
                                                    Bundle b = new Bundle(1);
                                                    if(webResponse.trim().equals("1")){
                                                        reqParams.clear();
                                                        reqParams.put("r","get_firm_account_details");
                                                        reqParams.put("email",farmEmail);
                                                        webResponse = WebHttpRequest.generalPost(uri,reqParams);
                                                        b.putString("farmRegisterResponse", "Account Registered in server but not yet recorded locally.");
                                                        ObjectMapper objectMapper = new ObjectMapper();
                                                        Farm farmDetails = objectMapper.readValue(webResponse,Farm.class);
                                                        //write account details to local database
                                                        if(DAOImpl.insertFarmDetails(farmDetails.getFarmServerId(),farmName,farmEmail,getSHA256(farmPwd),farmPhoneNo,district,plusCode) >= 1){
                                                            b.putString("farmRegisterResponse","Account Registered");
                                                        }
                                                    }else {
                                                        //Toast.makeText(context,"Account with this email exits or some other error occured.", Toast.LENGTH_SHORT).show();
                                                        b.putString("farmRegisterResponse","Account with this email exits or some other error occured.");
                                                    }

                                                    //create a message from the message handler to send it back to the main UI
                                                    Message msg = mHandler.obtainMessage();

                                                    //specify the type of message
                                                    msg.what = 1;

                                                    //attach the bundle to the message
                                                    msg.setData(b);

                                                    //send the message back to main UI thread
                                                    mHandler.sendMessage(msg);
                                                    run = false;
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                    webResponse = e.getMessage();

                                                }
                                            }
                                            notifyAll();
                                        } catch (Exception e) {
                                            webResponse = e.getMessage();
                                        }
                                    }

                                };

                                thread.start();
                                try {
                                    thread.join();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                }
            });
            ((Button) findViewById(R.id.btnToLogin)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    setContentView(R.layout.activity_main);
                    ((Button) findViewById(R.id.btnLogin)).setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {

                            EditText farmEmailField = findViewById(R.id.loginEmailField);
                            String farmEmail = farmEmailField.getText().toString();
                            EditText farmPwdField = findViewById(R.id.loginPwdField);
                            String farmPwd = farmPwdField.getText().toString();
                            //first download account details of the given email
                            Thread thread = new Thread()
                            {
                                @Override
                                public void run() {
                                    try {
                                        boolean run = true;
                                        while(run) {
                                            WebHttpRequest whr = new WebHttpRequest();
                                            try {
                                                Map<String, String> reqParams = new HashMap<>();
                                                reqParams.put("r","get_firm_account_details");
                                                reqParams.put("email", farmEmail);
                                                //RequestPackage requestPackage = new RequestPackage();
                                                //requestPackage.setMethod("POST");
                                                //requestPackage.setParams(reqParams);
                                                String uri = serverUriAndPort + "/farm";

                                                //the json of models.Farm.class
                                                webResponse = WebHttpRequest.generalPost(uri,reqParams);
                                                run = false;
                                                Bundle b = new Bundle(1);

                                                ObjectMapper objectMapper = new ObjectMapper();
                                                Farm farmDetails = new Farm();

                                                if(webResponse.trim().equals("no account with this email")){
                                                    b.putString("accountSearchResponse","It looks like account with this email doesn't exist.");
                                                }
                                                else{
                                                    farmDetails = objectMapper.readValue(webResponse,Farm.class);
                                                    b.putString("accountSearchResponse","Your email/password combination doesn't match");

                                                    //write account details to local database
                                                    if(farmDetails.getFarmPwd().equals(getSHA256(farmPwd))){
                                                        if(DAOImpl.insertFarmDetails(farmDetails.getFarmServerId(),farmDetails.getFarmName(),farmEmail,getSHA256(farmPwd),farmDetails.getFarmPhoneNo(),farmDetails.getDistrict(),farmDetails.getPlusCode()) >= 1){
                                                            b.putString("accountSearchResponse","Account found and details fetched");

                                                            //also fetch transactions and put in local db
                                                        }
                                                        else {
                                                            //Toast.makeText(context,"Account with this email exits or some other error occured.", Toast.LENGTH_SHORT).show();
                                                            b.putString("accountSearchResponse","Account found but local db error.");
                                                        }
                                                    }
                                                }



                                                //create a message from the message handler to send it back to the main UI
                                                Message msg = mHandler.obtainMessage();

                                                //specify the type of message
                                                msg.what = 2;

                                                //attach the bundle to the message
                                                msg.setData(b);

                                                //send the message back to main UI thread
                                                mHandler.sendMessage(msg);

                                                notifyAll();

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                webResponse = e.getMessage();

                                            }
                                        }

                                    } catch (Exception e) {
                                        webResponse = e.getMessage();
                                    }
                                }

                            };

                            thread.start();

                        }
                    });
                }
            });
        }
        else{
            setContentView(R.layout.activity_main);
            //fill account info from db
            TextView farmNameField = findViewById(R.id.loginFarmNameField);
            farmNameField.setText(DAOImpl.getFarmName());
            EditText farmEmailField = findViewById(R.id.loginEmailField);
            farmEmailField.setText(DAOImpl.getFarmEmail());
            EditText farmPwdField = findViewById(R.id.loginPwdField);
            ImageView profilePicView = findViewById(R.id.imageView);

            ((Button) findViewById(R.id.btnLogin)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    //Toast.makeText(context,DAOImpl.getFarmName(),Toast.LENGTH_LONG).show();
                    if(notFistLogin()){
                        //comapre hashed pwd with local db pwd
                        if(getSHA256(farmPwdField.getText().toString()).equals(DAOImpl.getFarmPwd())){
                            mainPane();
                        }
                        else{
                            Toast.makeText(context,"Incorrect password. Please, correct and try again",Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {

                        Toast.makeText(context,"app logic error", Toast.LENGTH_SHORT).show();
                    }

                }
            });

            
        }



    }
    private boolean isFirstSetup() {
        return this.dao.readAdminTable().getCount() == 0;
    }
    public void mainPane(){
        startActivity(new Intent(this, MainpaneActivity.class));
    }

    public static String getSHA256(String input){

        String toReturn = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.reset();
            digest.update(input.getBytes("utf8"));
            toReturn = String.format("%064x", new BigInteger(1, digest.digest()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return toReturn;
    }
    private boolean notFistLogin(){
        //check if local db pwd field >= 64
        return DAOImpl.checkIfPwdExists();
    }

    private void downloadAccountDetails(String farmEmail){

    }





}