package com.gyurme;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gyurme.dao.DAOImpl;
import com.gyurme.models.AnimalIssue;
import com.gyurme.models.Farm;
import com.gyurme.models.Transaction;
import com.gyurme.webrequests.WebHttpRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainpaneActivity extends AppCompatActivity {
    Context context = this;
    public static List<AnimalIssue> issueAlerts = new ArrayList<>();
    public static String alertText;
    public static Farm farmDetailsFromServer = new Farm();
    final Handler mHandler = new Handler(Looper.getMainLooper()){

        public void handleMessage(android.os.Message msg) {
            Bundle b;
            b=msg.getData();
            if(msg.what==3){

                if(b.getString("transBackupMsg").equals("Transactions successfully backed up.")){
                    Toast.makeText(context,b.getString("transBackupMsg"),Toast.LENGTH_SHORT).show();
                    backupBtn.setText(setTextToBackupBtn());
                }
                else{
                    Toast.makeText(context,b.getString("transBackupMsg"),Toast.LENGTH_SHORT).show();
                }
            }
            else if(msg.what == 8){
                if(b.getString("transBackupMsg").equals("new alerts available")){
                    alertBtn.setText("Outbreak Alert (" + issueAlerts.size() + ")");
                }
                else {
                    Toast.makeText(context,alertText,Toast.LENGTH_SHORT).show();
                }
            }
//            else if(msg.what == 11){
//                //analysis arrived
//                if(b.getString("transBackupMsg").equals("analysis arrived")){
//                    showAlert("Farm's Simple Financial Analysis:",alertText);
//                }
//                else {
//                    Toast.makeText(context,"Couldn't fetch financial analysis",Toast.LENGTH_SHORT).show();
//                }
//            }
            else{

            }
            super.handleMessage(msg);
        }
    };
    static String backedRow;
    public static Button backupBtn;
    public static Button alertBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getActionBar().setTitle("My Farm");
        getSupportActionBar().setTitle("My Farm: Dashboard");
        setContentView(R.layout.activity_mainpane);


        //get farm's server details for geo cordinates
        getSelfServerDetails();

        //look for new notifications on each load
        getDiseaseAlerts();

        alertBtn = (Button) findViewById(R.id.btnAlert);

        alertBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertPane();
            }
        });
        backupBtn = (Button) findViewById(R.id.btnRecordTransactions);
        backupBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                transactionsRecordPane();
            }
        });
        ((Button) findViewById(R.id.btnReportIncident)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reportIncidentPane();
            }
        });
        ((Button) findViewById(R.id.btnAnalysis)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"Fetching analysis, please wait",Toast.LENGTH_SHORT).show();
                getFinancialAnalysis();
            }
        });
        ((Button) findViewById(R.id.btnBackup)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                //server uri

                DAOImpl dAOImpl = new DAOImpl();
                dAOImpl.numberOfTransToBackup();
                Cursor rowsToBackup = dAOImpl.generalQuery("select * from transactions where backup_status = 'no'");
                List<Transaction> transactionsToBackup = new ArrayList<>();
                List<String> dbIdsOfTransToBackup = new ArrayList<>();
                String str = "";
                while (rowsToBackup.moveToNext()) {
                    dbIdsOfTransToBackup.add(rowsToBackup.getString(rowsToBackup.getColumnIndex("id")));
                    Transaction t = new Transaction();
                    t.setFarmServerId(DAOImpl.getFarmServerId());
                    t.setTransactionType(rowsToBackup.getString(rowsToBackup.getColumnIndex("transaction_type")));
                    t.setDateTime(rowsToBackup.getString(rowsToBackup.getColumnIndex("date")));
                    t.setBill(rowsToBackup.getString(rowsToBackup.getColumnIndex("bill")));
                    t.setTransactionName(rowsToBackup.getString(rowsToBackup.getColumnIndex("name")));
                    t.setRecieptAmount(rowsToBackup.getDouble(rowsToBackup.getColumnIndex("reciept_amount")));
                    t.setPaymentAmount(rowsToBackup.getDouble(rowsToBackup.getColumnIndex("payment_amount")));
                    t.setTransactionDetails(rowsToBackup.getString(rowsToBackup.getColumnIndex("explanation")));
                    transactionsToBackup.add(t);
                }

                if(transactionsToBackup.size() > 0){
                    Thread thread = new Thread()
                    {
                        String webResponse = "";

                        @Override
                        public void run() {
                            try {
                                boolean run = true;
                                while(run) {
                                    WebHttpRequest whr = new WebHttpRequest();
                                    ObjectMapper objectMapper = new ObjectMapper();
                                    String transToBackJson = "";
                                    try {
                                        transToBackJson = objectMapper.writeValueAsString(transactionsToBackup);
                                    } catch (JsonProcessingException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        Map<String, String> reqParams = new HashMap<>();
                                        reqParams.put("r","backup_transactions");
                                        reqParams.put("transactions",transToBackJson);
                                        String uri = MainActivity.serverUriAndPort + "/farm";
                                        webResponse = WebHttpRequest.generalPost(uri,reqParams);
                                        run = false; // to prevent looping of request
                                        Bundle b = new Bundle(1);
                                        if(webResponse.trim().equals("Backup Successful")){
                                            b.putString("transBackupMsg","server backup successful.");
                                            //update local status
                                            int localSuccess = 0;
                                            for(int i = 0; i < dbIdsOfTransToBackup.size(); i++ ){
                                                if(DAOImpl.updateBackedUpStatus(dbIdsOfTransToBackup.get(i)) > 0){
                                                    localSuccess =+ 1;
                                                }
                                                else{
                                                    b.putString("transBackupMsg","local db error.");
                                                    break;
                                                }
                                            }
                                            if(localSuccess == dbIdsOfTransToBackup.size()){
                                                b.putString("transBackupMsg","Transactions successfully backed up.");
                                            }
                                        }
                                        else {
                                            b.putString("transBackupMsg","Server/Remote backup failed.");
                                        }

                                        //create a message from the message handler to send it back to the main UI
                                        Message msg = mHandler.obtainMessage();

                                        //specify the type of message
                                        msg.what = 3;

                                        //attach the bundle to the message
                                        msg.setData(b);

                                        //send the message back to main UI thread
                                        mHandler.sendMessage(msg);
                                        run = false;
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        webResponse = e.getMessage();

                                    }
                                }

                            } catch (Exception e) {
                                webResponse = e.getMessage();
                            }
                        }

                    };

                    thread.start();
                }
                else{
                    Toast.makeText(context,"No transactions remaining to backup.",Toast.LENGTH_SHORT).show();
                }
            }
        });
        backupBtn = findViewById(R.id.btnBackup);
        backupBtn.setText(setTextToBackupBtn());


    }
    public void transactionsRecordPane(){
        startActivity(new Intent(this, RecordTransactionsActivity.class));
    }
    public void alertPane(){
        startActivity(new Intent(this, MapsActivity.class));
    }
    public void reportIncidentPane(){
        startActivity(new Intent(this, ReportIncidentActivity.class));
    }
    public static String setTextToBackupBtn() {
        String valueOf = String.valueOf(DAOImpl.getCountOfTransToBackup());
        return "Backup (" + valueOf + ")";
    }
    private void getDiseaseAlerts(){
        Thread thread = new Thread()
        {
            String webResponse = "";

            @Override
            public void run() {
                try {
                    boolean run = true;
                    while(run) {
                        WebHttpRequest whr = new WebHttpRequest();
                        ObjectMapper objectMapper = new ObjectMapper();
                        try {
                            Map<String, String> reqParams = new HashMap<>();
                            reqParams.put("r","farm_incident_alerts");
                            reqParams.put("requester_farm_id", DAOImpl.getFarmServerId());

                            String uri = MainActivity.serverUriAndPort + "/farm";
                            webResponse = WebHttpRequest.generalPost(uri,reqParams);
                            run = false; // to prevent looping of request
                            Bundle b = new Bundle(1);
                            issueAlerts.clear();
                            issueAlerts = objectMapper.readValue(webResponse,objectMapper.getTypeFactory().constructCollectionType(List.class, AnimalIssue.class));
                            alertText = webResponse;

                            if(issueAlerts.size() >= 1){
                                b.putString("transBackupMsg","new alerts available");
                            }
                            Message msg = mHandler.obtainMessage();


                            //specify the type of message
                            msg.what = 8;

                            //attach the bundle to the message
                            msg.setData(b);

                            //send the message back to main UI thread
                            mHandler.sendMessage(msg);
                            run = false;
                        } catch (Exception e) {
                            e.printStackTrace();
                            webResponse = e.getMessage();

                        }
                    }

                } catch (Exception e) {
                    webResponse = e.getMessage();
                }
            }

        };

        thread.start();
    }
    private void getSelfServerDetails(){
        Thread thread = new Thread()
        {
            String webResponse = "";

            @Override
            public void run() {
                try {
                    boolean run = true;
                    while(run) {
                        WebHttpRequest whr = new WebHttpRequest();
                        ObjectMapper objectMapper = new ObjectMapper();
                        try {
                            Map<String, String> reqParams = new HashMap<>();
                            reqParams.put("r","get_firm_account_details");
                            reqParams.put("email", DAOImpl.getFarmEmail());

                            String uri = MainActivity.serverUriAndPort + "/farm";
                            webResponse = WebHttpRequest.generalPost(uri,reqParams);
                            run = false; // to prevent looping of request
                            Bundle b = new Bundle(1);

                            farmDetailsFromServer = objectMapper.readValue(webResponse,Farm.class);


                            if(issueAlerts.size() >= 1){
                                b.putString("transBackupMsg","server userdetails arrived");
                            }
                            Message msg = mHandler.obtainMessage();


                            //specify the type of message
                            msg.what = 11;

                            //attach the bundle to the message
                            msg.setData(b);

                            //send the message back to main UI thread
                            mHandler.sendMessage(msg);
                            run = false;
                        } catch (Exception e) {
                            e.printStackTrace();
                            webResponse = e.getMessage();

                        }
                    }

                } catch (Exception e) {
                    webResponse = e.getMessage();
                }
            }

        };

        thread.start();
    }
    private void getFinancialAnalysis(){
        Thread thread = new Thread()
        {
            String webResponse = "";

            @Override
            public void run() {
                try {
                    boolean run = true;
                    while(run) {
                        WebHttpRequest whr = new WebHttpRequest();
                        ObjectMapper objectMapper = new ObjectMapper();
                        try {
                            Map<String, String> reqParams = new HashMap<>();
                            reqParams.put("r","get_financial_analysis");
                            reqParams.put("requester_farm_id", DAOImpl.getFarmServerId());

                            String uri = MainActivity.serverUriAndPort + "/farm";
                            webResponse = WebHttpRequest.generalPost(uri,reqParams);
                            run = false; // to prevent looping of request
                            Bundle b = new Bundle(1);
                            alertText = webResponse;

                            if(issueAlerts.size() >= 1){
                                b.putString("transBackupMsg","analysis arrived");
                            }
                            Message msg = mHandler.obtainMessage();


                            //specify the type of message
                            msg.what = 9;

                            //attach the bundle to the message
                            msg.setData(b);

                            //send the message back to main UI thread
                            mHandler.sendMessage(msg);
                            run = false;
                        } catch (Exception e) {
                            e.printStackTrace();
                            webResponse = e.getMessage();

                        }
                    }

                } catch (Exception e) {
                    webResponse = e.getMessage();
                }
            }

        };

        thread.start();
    }

    public void showAlert(String alertHeader, String alertBody) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle((CharSequence) alertHeader);
        builder.setMessage((CharSequence) alertBody);
        builder.show();
    }
}