package com.gyurme;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;

import android.database.Cursor;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.gyurme.dao.DAOImpl;
import com.gyurme.models.AnimalIssue;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //showAlert("Outbreaks alert", MainpaneActivity.alertText);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        String outbreak = "";
        for(int i = 0; i <  MainpaneActivity.issueAlerts.size();i++){
            AnimalIssue as = MainpaneActivity.issueAlerts.get(i);
            outbreak = outbreak + "Outbreak: " + as.getConfirmedIssue() + "\nLocation: " + as.getDistrict() + "\n..............................\n\n";
            // Add a marker in Sydney and move the camera
            LatLng location = new LatLng(Double.valueOf(as.getLatitude()), Double.valueOf(as.getLongitude()));
            mMap.addMarker(new MarkerOptions().position(location).title(as.getConfirmedIssue()));
        }

        // Add a marker in Sydney and move the camera
        LatLng self = new LatLng(Double.valueOf(MainpaneActivity.farmDetailsFromServer.getLatitude()),  Double.valueOf(MainpaneActivity.farmDetailsFromServer.getLongitude()));
        mMap.addMarker(new MarkerOptions().position(self).title("My Farm"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(self));
        showAlert("Be on alert, outbreaks have been confirmed:",outbreak);
    }

//    public void buildAndShowAlert() {
//        Cursor readTransactions = new DAOImpl().readTransactions(this.dbReadquery);
//        if (readTransactions.getCount() == 0) {
//            showAlert("data from table: transactions", "data query failed");
//            return;
//        }
//        StringBuffer stringBuffer = new StringBuffer();
//        while (readTransactions.moveToNext()) {
//            stringBuffer.append(String.valueOf(readTransactions.getString(0)) + "    __    b:" + String.valueOf(readTransactions.getString(1)) + "\n" + String.valueOf(readTransactions.getString(2)) + "\nNRs. " + String.valueOf(readTransactions.getString(3)) + "\n");
//            StringBuilder sb = new StringBuilder();
//            sb.append(String.valueOf(readTransactions.getString(4)));
//            sb.append("\n\n");
//            stringBuffer.append(sb.toString());
//        }
//        showAlert("Outbreaks alert", MainpaneActivity.alertText);
//    }

    public void showAlert(String str, String str2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle((CharSequence) str);
        builder.setMessage((CharSequence) str2);
        builder.show();
    }
}