package com.gyurme;

import android.content.ContentValues;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Misc {
    public static String dateToday() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    public static String dateTimeNow(){
        return String.valueOf(new Date().getTime());
    }

}
