package com.gyurme;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;

import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.gyurme.dao.DAOImpl;
import com.gyurme.dao.SqliteHelper;

import java.util.ArrayList;
import java.util.List;

public class RecordTransactionsActivity extends AppCompatActivity {

    private TextView mTextView;
    String bill;
    CheckBox billCheckBox;
    String billCheckBoxOption;
    Context context = this;
    EditText pOrPNameField;
    TextView payeeLabel;
    String transInOrOut; //amount_in or amount_out (of the business)
    String transType; //the option selected by user during recording
    public static List<String> transactionOptions = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getActionBar().setTitle("My Farm");
        getSupportActionBar().setTitle("My Farm");
        setContentView(R.layout.activity_record_transactions);
        final String dateToday = Misc.dateToday();
        ((TextView) findViewById(R.id.purchaseOrPayDateView)).setText(dateToday);
        pOrPNameField = (EditText) findViewById(R.id.pOrPNameField);
        final EditText amountField = (EditText) findViewById(R.id.pOrPAmountField);
        billCheckBox = (CheckBox) findViewById(R.id.pOrPBillCheckbox);
        final EditText transDetailsField = (EditText) findViewById(R.id.pOrPExplanationTextField);
        ((Button) findViewById(R.id.recordPorPBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Double amountOut = Double.valueOf(0);
                Double amountIn = Double.valueOf(0);
                if (billCheckBox.isEnabled() == false) {
                    bill = SqliteHelper.billNo;
                } else if (billCheckBox.isChecked()) {
                    bill = SqliteHelper.billYes;
                } else {
                    bill = SqliteHelper.billNo;
                }
                String transName = String.valueOf(RecordTransactionsActivity.this.pOrPNameField.getText());
                if (transInOrOut.equals("amount_in")) {
                    amountIn = Double.parseDouble(amountField.getText().toString());
                    amountOut = 0.0d;
                } else if(transInOrOut.equals("amount_out")) {
                    amountIn = 0.0d;
                    amountOut = Double.parseDouble(amountField.getText().toString());
                }
                else {
                    Toast.makeText(RecordTransactionsActivity.this.context, "Please choose appropriate transaction type option.", Toast.LENGTH_SHORT).show();
                    //RecordTransactionsActivity.this.finish();
                    return;
                }
                if(TextUtils.isEmpty(pOrPNameField.getText().toString())){
                    Toast.makeText(RecordTransactionsActivity.this.context, "Vital details about transaction should be compulsorily filled.", Toast.LENGTH_SHORT).show();
                    //RecordTransactionsActivity.this.finish();
                    return;
                }
                String transDetails = String.valueOf(transDetailsField.getText());
                DAOImpl dAOImpl = new DAOImpl();
                long recordTransaction = dAOImpl.recordTransaction(Misc.dateTimeNow(), transType, bill, transName, amountIn, amountOut, transDetails);
                //Toast.makeText(RecordTransactionsActivity.this.context, String.valueOf(recordTransaction), Toast.LENGTH_LONG).show();
                if (recordTransaction == -1) {
                    Toast.makeText(RecordTransactionsActivity.this.context, "Entry recording failed", Toast.LENGTH_SHORT).show();
                    return;
                }
                //Context context = RecordTransactionsActivity.this.context;
                Toast.makeText(context, "Entry recording successful!: " + recordTransaction, Toast.LENGTH_SHORT).show();
                if (dAOImpl.newNumberOfTransBackup() > 0) {
                    MainpaneActivity.backupBtn.setText(MainpaneActivity.setTextToBackupBtn());
                    RecordTransactionsActivity.this.finish();
                    return;
                }
                Toast.makeText(RecordTransactionsActivity.this.context, "Fatal error. Didn't increment backup count", Toast.LENGTH_LONG).show();
                //Toast.makeText(RecordTransactionsActivity.this.context, "test", Toast.LENGTH_LONG).show();
            }
        });
        transactionOptions.clear();
        Cursor readTransactions = new DAOImpl().readTransactionTypes("select * from transaction_types");
        while (readTransactions.moveToNext()) {
            Toast.makeText(context,readTransactions.getString(1),Toast.LENGTH_LONG);
            transactionOptions.add(readTransactions.getString(1));
        }
        Spinner spinner = (Spinner) findViewById(R.id.pOrPSpinner);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this.context,   android.R.layout.simple_spinner_item, transactionOptions);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinner.setAdapter(spinnerArrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                transType = parent.getSelectedItem().toString();
                String query = "select amount_in_out from transaction_types where transaction_name = '" + transType + "';";
                Cursor cursor = new DAOImpl().generalQuery(query);
                while (cursor.moveToNext()){
                    transInOrOut = cursor.getString(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

}
//    int j = 0; // the position of prepared_statement ?
//            for(int i = 0; i < transToBackup.size(); i++){
//        Transaction t = transToBackup.get(i);
//        ps.setString(j+1, t.getFarmServerId());
//        ps.setTimestamp(j+2, new java.sql.Timestamp(Long.valueOf(t.getDateTime())));
//        ps.setString(j+3, t.getTransactionType());
//        ps.setString(j+4, t.getBill());
//        ps.setString(j+5, t.getTransactionName());
//        ps.setDouble(j+6, t.getRecieptAmount());
//        ps.setDouble(j+7, t.getPaymentAmount());
//        ps.setString(j+8, t.getTransactionDetails());
//        }