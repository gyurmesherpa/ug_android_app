package com.gyurme;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gyurme.dao.DAOImpl;
import com.gyurme.webrequests.WebHttpRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReportIncidentActivity extends AppCompatActivity {
    Context context = this;
    final Handler mHandler = new Handler(Looper.getMainLooper()){

        public void handleMessage(android.os.Message msg) {
            Bundle b;
            if(msg.what==4){
                b=msg.getData();
                if(b.getString("transBackupMsg").equals("animals list fetched")){
                    //populate animals spinner
                    populateAnimalsSpinner();
                }
                else{
                    Toast.makeText(context,"Failed to fetch animals list from server",Toast.LENGTH_SHORT).show();
                }
            }
            else if(msg.what==5){
                b = msg.getData();

                if(b.getString("transBackupMsg").equals("animals diseases fetched")){
                    //populate animals spinner
                    populateDiseasesSpinner();
                }
                else{
                    Toast.makeText(context,"Failed to fetch animal diseases list from server",Toast.LENGTH_SHORT).show();
                }
            }
            else if(msg.what==6){
                b = msg.getData();
                if(b.getString("transBackupMsg").equals("successfully reported")){
                    //populate animals spinner
                    Toast.makeText(context, "Successfully reported", Toast.LENGTH_SHORT).show();
                    ReportIncidentActivity.this.finish();
                }
                else{
                    Toast.makeText(context,"Failed to report incident",Toast.LENGTH_SHORT).show();
                }
            }
            else{

            }
            super.handleMessage(msg);
        }
    };

    private List<String> animalsList = new ArrayList<>(); //from server
    private List<String> animalDiseasesList = new ArrayList<>(); //from server
    private EditText dateOnSinceField;
    private Spinner animalsSpinner;
    private Spinner diseasesSpinner;
    private EditText symptomsField;
    private EditText subjectField;
    String selectedAnimal = null;
    String selectedAnimalIssue = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("My Farm: Report Incident");
        setContentView(R.layout.activity_report_incident);
        dateOnSinceField = findViewById(R.id.incidentDateField);
        dateOnSinceField.setText(Misc.dateToday());
        dateOnSinceField = findViewById(R.id.incidentDateField);
        symptomsField = findViewById(R.id.symptomsField);
        subjectField = findViewById(R.id.reportSubjectField);
        String reportOn = Misc.dateToday();
        animalsSpinner = (Spinner) findViewById(R.id.spinnerAnimalType);
        fetchAnimalSpinnerData();
        animalsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedAnimal = parent.getSelectedItem().toString();
                fetchAnimalDiseasesData(selectedAnimal);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        diseasesSpinner = findViewById(R.id.spinnerSuspectedIncident);
        diseasesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedAnimalIssue = parent.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
       ((Button) findViewById(R.id.btnSubmitIncident)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Thread thread = new Thread()
                {
                    String webResponse = "";

                    @Override
                    public void run() {
                        try {
                            boolean run = true;
                            while(run) {
                                WebHttpRequest whr = new WebHttpRequest();
                                ObjectMapper objectMapper = new ObjectMapper();

                                try {
                                    Map<String, String> reqParams = new HashMap<>();
                                    reqParams.put("r","report_farm_incident");
                                    reqParams.put("reporting_farm_id",DAOImpl.getFarmServerId());
                                    reqParams.put("reporting_date",Misc.dateTimeNow());
                                    reqParams.put("incident_of_or_since",dateOnSinceField.getText().toString());
                                    reqParams.put("animal",selectedAnimal);
                                    reqParams.put("issue",selectedAnimalIssue);
                                    reqParams.put("symptoms",symptomsField.getText().toString());
                                    reqParams.put("report_subject",subjectField.getText().toString());
                                    String uri = MainActivity.serverUriAndPort + "/farm";
                                    webResponse = WebHttpRequest.generalPost(uri,reqParams);
                                    run = false; // to prevent looping of request
                                    Bundle b = new Bundle(1);

                                    if(webResponse.trim().equals("successfully reported")){
                                        b.putString("transBackupMsg","successfully reported");
                                    }
                                    else{
                                        b.putString("transBackupMsg","reporting failed");
                                    }

                                    //create a message from the message handler to send it back to the main UI
                                    Message msg = mHandler.obtainMessage();

                                    //specify the type of message
                                    msg.what = 6;

                                    //attach the bundle to the message
                                    msg.setData(b);

                                    //send the message back to main UI thread
                                    mHandler.sendMessage(msg);
                                    run = false;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    webResponse = e.getMessage();

                                }
                            }

                        } catch (Exception e) {
                            webResponse = e.getMessage();
                        }
                    }

                };

                thread.start();
            }
        });




    }

    private void fetchAnimalSpinnerData(){
        Thread thread = new Thread()
        {
            String webResponse = "";

            @Override
            public void run() {
                try {
                    boolean run = true;
                    while(run) {
                        WebHttpRequest whr = new WebHttpRequest();
                        ObjectMapper objectMapper = new ObjectMapper();

                        try {
                            Map<String, String> reqParams = new HashMap<>();
                            reqParams.put("r","get_all_animals_list");
                            String uri = MainActivity.serverUriAndPort + "/farm";
                            webResponse = WebHttpRequest.generalPost(uri,reqParams);
                            run = false; // to prevent looping of request
                            Bundle b = new Bundle(1);

                            animalsList = objectMapper.readValue(webResponse, new TypeReference<List<String>>() {});

                            b.putString("transBackupMsg","animals list fetched");

                            //create a message from the message handler to send it back to the main UI
                            Message msg = mHandler.obtainMessage();

                            //specify the type of message
                            msg.what = 4;

                            //attach the bundle to the message
                            msg.setData(b);

                            //send the message back to main UI thread
                            mHandler.sendMessage(msg);
                            run = false;
                        } catch (Exception e) {
                            e.printStackTrace();
                            webResponse = e.getMessage();

                        }
                    }

                } catch (Exception e) {
                    webResponse = e.getMessage();
                }
            }

        };

        thread.start();
    }
    private void fetchAnimalDiseasesData(String animal){
        Thread thread = new Thread()
        {
            String webResponse = "";

            @Override
            public void run() {
                try {
                    boolean run = true;
                    while(run) {
                        WebHttpRequest whr = new WebHttpRequest();
                        ObjectMapper objectMapper = new ObjectMapper();

                        try {
                            Map<String, String> reqParams = new HashMap<>();
                            reqParams.put("r","get_specific_animal_diseases");
                            reqParams.put("animal",animal);
                            String uri = MainActivity.serverUriAndPort + "/farm";
                            webResponse = WebHttpRequest.generalPost(uri,reqParams);
                            run = false; // to prevent looping of request
                            Bundle b = new Bundle(1);

                            animalDiseasesList = objectMapper.readValue(webResponse, new TypeReference<List<String>>() {});

                            b.putString("transBackupMsg","animals diseases fetched");

                            //create a message from the message handler to send it back to the main UI
                            Message msg = mHandler.obtainMessage();

                            //specify the type of message
                            msg.what = 5;

                            //attach the bundle to the message
                            msg.setData(b);

                            //send the message back to main UI thread
                            mHandler.sendMessage(msg);
                            run = false;
                        } catch (Exception e) {
                            e.printStackTrace();
                            webResponse = e.getMessage();

                        }
                    }

                } catch (Exception e) {
                    webResponse = e.getMessage();
                }
            }

        };

        thread.start();
    }
    private void populateAnimalsSpinner(){
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this.context,   android.R.layout.simple_spinner_item, animalsList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        animalsSpinner.setAdapter(spinnerArrayAdapter);
    }
    private void populateDiseasesSpinner(){
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this.context,   android.R.layout.simple_spinner_item, animalDiseasesList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        diseasesSpinner.setAdapter(spinnerArrayAdapter);
    }
}