package com.gyurme;

import android.content.Context;
import android.widget.Toast;

public class ToastMessage {
    public static void message(Context context, String str) {
        Toast.makeText(context, str, 0).show();
    }
}
