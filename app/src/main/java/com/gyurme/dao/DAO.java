package com.gyurme.dao;

import android.content.Context;
import android.database.Cursor;

public interface DAO {
    long backedupTillRow();

    void changePwd(String str);

    String loginPassword();

    long newBackedupTillRow(long j);

    int newNumberOfTransBackup();

    int numberOfTransToBackup();

    Cursor readAdminTable();

    Cursor readTransactions(String str);

    long recordPayment(String str, String str2, double d);

    void recordReciept();

    long recordTransaction(String str, String str2, String str3, String str4, Double d, Double d2, String str5);

    Cursor rowsToBackup(long j);

    long setDefaultAppEnvironment();

    void insertInitialTransactionTypes();

    void trialEntry(Context context);

    Cursor trialRead();
}
