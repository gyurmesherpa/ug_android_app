package com.gyurme.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.HashMap;
import java.util.Map;

public class DAOImpl extends AppCompatActivity implements DAO {
    public static SQLiteDatabase sqLiteDatabase;

    public void recordReciept() {
    }

    public Cursor readAdminTable() {
        return getQueryData("select * from app_admin");
    }

    public long setDefaultAppEnvironment() {
        ContentValues contentValues = new ContentValues();
        contentValues.clear();
        contentValues.put("particular", "num_of_backups_todo");
        contentValues.put("value", "0");
        sqLiteDatabase.insert("app_admin", (String) null, contentValues);
        contentValues.clear();
        contentValues.put("particular", "backedupTillRow");
        contentValues.put("value", "0");
        return sqLiteDatabase.insert("app_admin", (String) null, contentValues);
    }
    public void insertInitialTransactionTypes(){
        sqLiteDatabase.execSQL("delete from transaction_types");
        ContentValues contentValues = new ContentValues();
        contentValues.put("transaction_name", "Select transaction");
        contentValues.put("amount_in_out", "none");
        sqLiteDatabase.insert("transaction_types", (String) null, contentValues);
        contentValues.put("transaction_name", "Received");
        contentValues.put("amount_in_out", "amount_in");
        sqLiteDatabase.insert("transaction_types", (String) null, contentValues);
        contentValues.put("transaction_name", "Sale");
        contentValues.put("amount_in_out", "amount_in");
        sqLiteDatabase.insert("transaction_types", (String) null, contentValues);
        contentValues.put("transaction_name", "Payment");
        contentValues.put("amount_in_out", "amount_out");
        sqLiteDatabase.insert("transaction_types", (String) null, contentValues);
        contentValues.put("transaction_name", "Purchase");
        contentValues.put("amount_in_out", "amount_out");
        sqLiteDatabase.insert("transaction_types", (String) null, contentValues);

    }

    private static Cursor getQueryData(String str) {
        return sqLiteDatabase.rawQuery(str, (String[]) null);
    }
    public static long insertFarmDetails(String farmServerId, String farmName, String email, String pwd, String phoneNo, String district, String plusCode){
        ContentValues contentValues = new ContentValues();
        contentValues.put("particular", "farm_server_id");
        contentValues.put("value", farmServerId);
        sqLiteDatabase.insert("app_admin", (String) null, contentValues);
        contentValues.put("particular", "farm_name");
        contentValues.put("value", farmName);
        sqLiteDatabase.insert("app_admin", (String) null, contentValues);
        contentValues.put("particular", "farm_email");
        contentValues.put("value", email);
        sqLiteDatabase.insert("app_admin", (String) null, contentValues);
        contentValues.put("particular", "pwd");
        contentValues.put("value", pwd);
        sqLiteDatabase.insert("app_admin", (String) null, contentValues);
        contentValues.put("particular", "district");
        contentValues.put("value", district);
        sqLiteDatabase.insert("app_admin", (String) null, contentValues);
        contentValues.put("particular", "plus_code");
        contentValues.put("value", plusCode);
        sqLiteDatabase.insert("app_admin", (String) null, contentValues);
        contentValues.put("particular", "phoneNo");
        contentValues.put("value", phoneNo);
        return sqLiteDatabase.insert("app_admin", (String) null, contentValues);
    }
    public long recordPayment(String str, String str2, double d) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", str);
        contentValues.put("reciepent_name", str2);
        contentValues.put("amount", Double.valueOf(d));
        return sqLiteDatabase.insert("payments", (String) null, contentValues);
    }

    public void trialEntry(Context context) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("amount", 25);
        long insert = sqLiteDatabase.insert("trial", (String) null, contentValues);
        if (insert == -1) {
            Toast.makeText(context, "Entry recording failed", Toast.LENGTH_SHORT).show();
            return;
        }
        Toast.makeText(context, "Entry recording successful!: " + insert, Toast.LENGTH_SHORT).show();
    }

    public Cursor trialRead() {
        return getQueryData("select amount from trial");
    }

    public long recordTransaction(String date, String transType, String billYesNo, String transactionName, Double amountIn, Double amountOut, String transactionsDetails) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", date);
        contentValues.put("transaction_type", transType);
        contentValues.put("bill", billYesNo);
        contentValues.put("name", transactionName);
        contentValues.put("reciept_amount", amountIn);
        contentValues.put("payment_amount", amountOut);
        contentValues.put("explanation", transactionsDetails);
        contentValues.put("backup_status", "no");
        return sqLiteDatabase.insert("transactions", (String) null, contentValues);
    }

    public String loginPassword() {
        Cursor queryData = getQueryData("select value from app_admin where particular = 'pwd'");
        String str = null;
        if (queryData.getCount() != 0) {
            while (queryData.moveToNext()) {
                str = String.valueOf(queryData.getString(0));
            }
        }
        return str;
    }

    public static String getFarmName(){
        Cursor queryData = getQueryData("select value from app_admin where particular = 'farm_name'");
        String str = null;
        if (queryData.getCount() != 0) {
            while (queryData.moveToNext()) {
                str = String.valueOf(queryData.getString(0));
            }
        }
        return str;
    }

    public static String getFarmEmail(){
        Cursor queryData = getQueryData("select value from app_admin where particular = 'farm_email'");
        String str = null;
        if (queryData.getCount() != 0) {
            while (queryData.moveToNext()) {
                str = String.valueOf(queryData.getString(0));
            }
        }
        return str;
    }

    public static String getFarmPhoneNo(){
        Cursor queryData = getQueryData("select value from app_admin where particular = 'phoneNo'");
        String str = null;
        if (queryData.getCount() != 0) {
            while (queryData.moveToNext()) {
                str = String.valueOf(queryData.getString(0));
            }
        }
        return str;
    }

    public static String getFarmServerId(){
        Cursor queryData = getQueryData("select value from app_admin where particular = 'farm_server_id'");
        String str = null;
        if (queryData.getCount() != 0) {
            while (queryData.moveToNext()) {
                str = String.valueOf(queryData.getString(0));
            }
        }
        return str;
    }

    public static String getFarmPwd(){
        Cursor queryData = getQueryData("select value from app_admin where particular = 'pwd'");
        String str = null;
        if (queryData.getCount() != 0) {
            while (queryData.moveToNext()) {
                str = String.valueOf(queryData.getString(0));
            }
        }
        return str;
    }

    public static boolean checkIfPwdExists(){
        Cursor queryData = getQueryData("select value from app_admin where particular = 'pwd'");
        if (queryData.getCount() != 0) {
           return true;
        }
        return false;
    }

    public void changePwd(String str) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("value", str);
        sqLiteDatabase.update("app_admin", contentValues, "particular = 'login_pass'", (String[]) null);
    }

    public Cursor readTransactions(String str) {
        return getQueryData(str);
    }

    public Cursor readTransactionTypes(String readQueary) {return getQueryData(readQueary);}

    public Cursor generalQuery(String query){return getQueryData(query);}

    public static Cursor generalReadQuery(String query){return getQueryData(query);}

    public int numberOfTransToBackup() {
        Cursor queryData = getQueryData("select value from app_admin where particular = 'num_of_backups_todo'");
        int i = 0;
        while (queryData.moveToNext()) {
            i = Integer.parseInt(queryData.getString(0));
        }
        return i;
    }

    public int newNumberOfTransBackup() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("value", String.valueOf(numberOfTransToBackup() + 1));
        return sqLiteDatabase.update("app_admin", contentValues, "particular = 'num_of_backups_todo'", (String[]) null);
    }

    public long backedupTillRow() {
        Cursor queryData = getQueryData("select value from app_admin where particular = 'backedupTillRow'");
        long j = 0;
        while (queryData.moveToNext()) {
            j = (long) Integer.parseInt(queryData.getString(0));
        }
        return j;
    }

    public long newBackedupTillRow(long j) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("value", String.valueOf(j));
        sqLiteDatabase.update("app_admin", contentValues, "particular = 'backedupTillRow'", (String[]) null);
        contentValues.clear();
        contentValues.put("value", String.valueOf(numberOfTransToBackup() - 1));
        sqLiteDatabase.update("app_admin", contentValues, "particular = 'num_of_backups_todo'", (String[]) null);
        return backedupTillRow();
    }

    public static int updateBackedUpStatus(String rowId){
        ContentValues contentValues = new ContentValues();
        contentValues.put("backup_status", "yes");
        return sqLiteDatabase.update("transactions", contentValues, "id = '"+ rowId +"'", (String[]) null);

    }

    public static int getCountOfTransToBackup(){
        Cursor queryData = getQueryData("select id from transactions where backup_status = 'no'");
        int j = queryData.getCount();
        //queryData.close();
        return j;
    }

    public Cursor rowsToBackup(long j) {
        return getQueryData("select * from transactions where id > " + j);
    }
}
