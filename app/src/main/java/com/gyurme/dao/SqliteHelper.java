package com.gyurme.dao;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.gyurme.ToastMessage;

public class SqliteHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "farmrecords.db";
    private static final String TABLE_APP_ADMIN = "app_admin";
    private static final String TABLE_TRANSACTIONS = "transactions";
    private static final String TABLE_TRIAL = "trial";
    public static final String billNo = "no";
    public static final String billYes = "yes";
    private static int dbVersion = 40;
    public static final String transTypePay = "pay";
    public static final String transTypePurchase = "purchase";
    public static final String transTypeReciept = "reciept";
    private Context context;

    public static void increaseDbVersion() {
    }

    public SqliteHelper(Context context2) {
        super(context2, DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, dbVersion);
        this.context = context2;
        System.out.println("constructor called");
    }

    public Context getSqliteHelperContext() {
        return this.context;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        try {
            sQLiteDatabase.execSQL("drop table if exists transactions");
            sQLiteDatabase.execSQL("drop table if exists transaction_types");
            sQLiteDatabase.execSQL("drop table if exists trial");
            sQLiteDatabase.execSQL("drop table if exists app_admin");
            sQLiteDatabase.execSQL("create table trial (id integer primary key autoincrement, amount integer);");
            sQLiteDatabase.execSQL("create table app_admin (id integer primary key autoincrement, particular VARCHAR(100), value VARCHAR(100))");
            sQLiteDatabase.execSQL("create table transaction_types (id integer primary key autoincrement, transaction_name VARCHAR(100), amount_in_out VARCHAR(10))");
            sQLiteDatabase.execSQL("create table transactions (id integer primary key autoincrement, date VARCHAR(20), transaction_type VARCHAR(100), bill VARCHAR(10), name VARCHAR(100), reciept_amount decimal, payment_amount decimal, explanation TEXT, backup_status VARCHAR(3));");
            ToastMessage.message(this.context, "On create called");
        } catch (SQLException e) {
            Context context2 = this.context;
            ToastMessage.message(context2, "db call error");
            e.printStackTrace();
        }
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        try {
            sQLiteDatabase.execSQL("drop table if exists transactions");
            sQLiteDatabase.execSQL("drop table if exists transaction_types");
            sQLiteDatabase.execSQL("drop table if exists trial");
            sQLiteDatabase.execSQL("drop table if exists app_admin");
            ToastMessage.message(this.context, "DB onUpgrade called");
            onCreate(sQLiteDatabase);
        } catch (SQLException e) {
            Context context2 = this.context;
            ToastMessage.message(context2, "db creation failure");
            e.printStackTrace();
        }
    }
}
