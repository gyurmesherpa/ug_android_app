package com.gyurme.models;

import java.util.List;

public class AnimalIssue {
    String issueId;
    String animal;
    String issueName; //as reported
    List<String> issueSymptoms;
    String issueSypmtomsText;
    List<String> immagesSentByReporter;
    String issueConfirmationStatus; //confirmed or unconfirmed
    String confirmedIssue;
    String confirmedBy; //expert db id
    String farmServerId; //where incident happened
    String farmPhoneNo;
    String district;
    String plusCode;
    String latitude;
    String longitude;
    String reportedAt; //time
    String incidentOf; //date
    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    public String getAnimal() {
        return animal;
    }

    public void setAnimal(String animal) {
        this.animal = animal;
    }

    public String getIssueName() {
        return issueName;
    }

    public void setIssueName(String issueName) {
        this.issueName = issueName;
    }

    public List<String> getIssueSymptoms() {
        return issueSymptoms;
    }

    public void setIssueSymptoms(List<String> issueSymptoms) {
        this.issueSymptoms = issueSymptoms;
    }

    public List<String> getImmagesSentByReporter() {
        return immagesSentByReporter;
    }

    public void setImmagesSentByReporter(List<String> immagesSentByReporter) {
        this.immagesSentByReporter = immagesSentByReporter;
    }

    public String getConfirmedBy() {
        return confirmedBy;
    }

    public void setConfirmedBy(String confirmedBy) {
        this.confirmedBy = confirmedBy;
    }

    public String getIssueSypmtomsText() {
        return issueSypmtomsText;
    }

    public void setIssueSypmtomsText(String issueSypmtomsText) {
        this.issueSypmtomsText = issueSypmtomsText;
    }

    public String getIssueConfirmationStatus() {
        return issueConfirmationStatus;
    }

    public void setIssueConfirmationStatus(String issueConfirmationStatus) {
        this.issueConfirmationStatus = issueConfirmationStatus;
    }

    public String getFarmServerId() {
        return farmServerId;
    }

    public void setFarmServerId(String farmServerId) {
        this.farmServerId = farmServerId;
    }

    public String getFarmPhoneNo() {
        return farmPhoneNo;
    }

    public void setFarmPhoneNo(String farmPhoneNo) {
        this.farmPhoneNo = farmPhoneNo;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPlusCode() {
        return plusCode;
    }

    public void setPlusCode(String plusCode) {
        this.plusCode = plusCode;
    }

    public String getConfirmedIssue() {
        return confirmedIssue;
    }

    public void setConfirmedIssue(String confirmedIssue) {
        this.confirmedIssue = confirmedIssue;
    }

    public String getReportedAt() {
        return reportedAt;
    }

    public void setReportedAt(String reportedAt) {
        this.reportedAt = reportedAt;
    }

    public String getIncidentOf() {
        return incidentOf;
    }

    public void setIncidentOf(String incidentOf) {
        this.incidentOf = incidentOf;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
