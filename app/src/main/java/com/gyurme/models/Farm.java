package com.gyurme.models;

public class Farm {
    private String farmServerId;
    private String farmName;
    private String farmEmail;
    private String farmPwd;
    private String farmPhoneNo;
    private String district;
    private String plusCode;
    private String latitude;
    private String longitude;

    public String getFarmServerId() {
        return farmServerId;
    }

    public void setFarmServerId(String farmServerId) {
        this.farmServerId = farmServerId;
    }

    public String getFarmName() {
        return farmName;
    }

    public void setFarmName(String farmName) {
        this.farmName = farmName;
    }

    public String getFarmEmail() {
        return farmEmail;
    }

    public void setFarmEmail(String farmEmail) {
        this.farmEmail = farmEmail;
    }

    public String getFarmPwd() {
        return farmPwd;
    }

    public void setFarmPwd(String farmPwd) {
        this.farmPwd = farmPwd;
    }

    public String getFarmPhoneNo() {
        return farmPhoneNo;
    }

    public void setFarmPhoneNo(String farmPhoneNo) {
        this.farmPhoneNo = farmPhoneNo;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPlusCode() {
        return plusCode;
    }

    public void setPlusCode(String plusCode) {
        this.plusCode = plusCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
