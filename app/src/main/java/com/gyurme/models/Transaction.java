package com.gyurme.models;

public class Transaction {
    String farmServerId;
    String dateTime;
    String transactionType;
    String bill;
    String transactionName; //particular
    Double recieptAmount;
    Double paymentAmount;
    String transactionDetails;

    public String getFarmServerId() {
        return farmServerId;
    }

    public void setFarmServerId(String farmServerId) {
        this.farmServerId = farmServerId;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getBill() {
        return bill;
    }

    public void setBill(String bill) {
        this.bill = bill;
    }

    public String getTransactionName() {
        return transactionName;
    }

    public void setTransactionName(String transactionName) {
        this.transactionName = transactionName;
    }

    public Double getRecieptAmount() {
        return recieptAmount;
    }

    public void setRecieptAmount(Double recieptAmount) {
        this.recieptAmount = recieptAmount;
    }

    public Double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(Double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getTransactionDetails() {
        return transactionDetails;
    }

    public void setTransactionDetails(String transactionDetails) {
        this.transactionDetails = transactionDetails;
    }
}
