package com.gyurme.webrequests;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

public class AsyncWebTasks extends AsyncTask<RequestPackage, String, String> {
    //web req 1
    //web req 2
    @Override
    protected String doInBackground(RequestPackage... params) {
        return HttpManager.getData(params[0]);
    }

    //The String that is returned in the doInBackground() method is sent to the
    // onPostExecute() method below. The String should contain JSON data.
    @Override
    protected void onPostExecute(String result) {
        try {
            //We need to convert the string in result to a JSONObject
            JSONObject jsonObject = new JSONObject(result);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
