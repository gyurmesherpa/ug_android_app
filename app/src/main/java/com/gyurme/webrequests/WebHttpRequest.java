package com.gyurme.webrequests;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.json.JSONObject;

class ParameterStringBuilder {
    public static String getParamsString(Map<String, String> params)
            throws UnsupportedEncodingException{
        StringBuilder result = new StringBuilder();

        for (Map.Entry<String, String> entry : params.entrySet()) {
            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            result.append("&");
        }

        String resultString = result.toString();
        return resultString.length() > 0
                ? resultString.substring(0, resultString.length() - 1)
                : resultString;
    }
}

public class WebHttpRequest {
    String serverIpAndPort;
    public String postQuestion(JSONObject quoJson) throws Exception {

        serverIpAndPort = "localhost";//String.valueOf(OptionsPane.ipField.getText());

        HttpURLConnection http = null;
        String result = "";
        try {
            URL url = new URL("http://"+serverIpAndPort+"/general_projects/cis_php_web/");
            http = (HttpURLConnection) url.openConnection();
            //http.setRequestProperty("trialKey","value");
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setDoInput(true);

            Map<String, String> parameters = new HashMap<>();
            parameters.put("r", "postQuestion");
            parameters.put("teacherId", "215");
            parameters.put("question",quoJson.toString());

            OutputStream ops = http.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(ops,"UTF-8"));
            //String data = URLEncoder.encode("reqId","UTF-8")+"="+URLEncoder.encode(requestString,"UTF-8");
            writer.write(ParameterStringBuilder.getParamsString(parameters)); //commented after putting it to web project
            //operationMessage = data;
            writer.flush();
            writer.close();
            ops.close();
            http.connect();

            InputStream ips = http.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(ips,"ISO-8859-1"));
            String line = "";
            while ((line = reader.readLine()) != null){
                result += line;
            }
            reader.close();
            ips.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            if(http != null) // Make sure the connection is not null.
                http.disconnect();
        }
        //operationMessage = result;
        //System.out.println(result);
        return result;
    }
    public String trialGet() throws Exception {

        serverIpAndPort = "google.com";

        HttpURLConnection http = null;
        String result = "";
        try {
            URL url = new URL("http://"+serverIpAndPort+"/");
            http = (HttpURLConnection) url.openConnection();
            //http.setRequestProperty("trialKey","value");
            http.setRequestMethod("GET");
            http.setDoOutput(true);
            http.setDoInput(true);

//            Map<String, String> parameters = new HashMap<>();
//            parameters.put("r", "postQuestion");
//            parameters.put("teacherId", "215");
//            parameters.put("question",quoJson.toString());

            OutputStream ops = http.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(ops,"UTF-8"));
            writer.flush();
            writer.close();
            ops.close();
            http.connect();

            InputStream ips = http.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(ips,"ISO-8859-1"));
            String line = "";
            while ((line = reader.readLine()) != null){
                result += line;
            }
            reader.close();
            ips.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            if(http != null) // Make sure the connection is not null.
                http.disconnect();
        }
        //operationMessage = result;
        //System.out.println(result);
        return result;
    }
    public static String generalPost(String uri, Map<String, String> parameters) throws Exception {

        HttpURLConnection http = null;
        String result = "";
        try {
            URL url = new URL(uri);
            http = (HttpURLConnection) url.openConnection();
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setDoInput(true);

            OutputStream ops = http.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(ops,"UTF-8"));
            writer.write(ParameterStringBuilder.getParamsString(parameters)); //commented after putting it to web project
            writer.flush();
            writer.close();
            ops.close();
            http.connect();

            InputStream ips = http.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(ips,"ISO-8859-1"));
            String line = "";
            while ((line = reader.readLine()) != null){
                result += line;
            }
            reader.close();
            ips.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            if(http != null) // Make sure the connection is not null.
                http.disconnect();
        }
        //operationMessage = result;
        //System.out.println(result);
        return result;
    }
}


